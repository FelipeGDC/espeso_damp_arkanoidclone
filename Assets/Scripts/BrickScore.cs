﻿using UnityEngine;
using System.Collections;

public class BrickScore : MonoBehaviour {

    [SerializeField]
    private int score;

    private PlayerInfo playerInfo;
    
	void Start () {
        playerInfo = FindObjectOfType<PlayerInfo>();
        print(playerInfo);
	}

    void OnDestroy() {
        playerInfo.score += score;
    }
}
