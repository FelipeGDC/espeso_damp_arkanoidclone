﻿using UnityEngine;
using System.Collections;

public class PufParticulas : MonoBehaviour {

    private ParticleSystem ps;

	// Use this for initialization
	void Start () {
        ps = GetComponentInChildren<ParticleSystem>();
    }
	
    void OnCollisionEnter2D(Collision2D collision)
    {
        //ps.transform.position = new Vector3(
        //    collision.transform.position.x,
        //    collision.transform.position.y + 
        //        ((collision.rigidbody.velocity.y > 0) ? 
        //            -collision.collider.bounds.size.y/2 : collision.collider.bounds.size.y / 2),
        //    transform.position.z - 0.5f
        //    );
        ps.transform.position = new Vector3(
            collision.contacts[0].point.x,
            collision.contacts[0].point.y,
            transform.position.z + 1.0f);  
        ps.Play();
    }
}
