﻿using System;
using UnityEngine;
using System.Collections;

public class Persistant : MonoBehaviour {

    private float _lifeTime;
    public float lifeTime {
        get { return _lifeTime; }
    }

    virtual protected void Awake() {
        Type managerType = this.GetType();
        _lifeTime = 0f;
        Persistant[] persistantObjects = FindObjectsOfType<Persistant>();
		// Filter per actual type
		ArrayList managers =  new ArrayList(); 
		foreach (Persistant curObject in persistantObjects) {
			if (curObject.GetType() == managerType) {
				managers.Add(curObject);
			}
		}
		if (managers.Count == 1) {
            GameObject.DontDestroyOnLoad(gameObject);
        }
        else {
			foreach (Persistant curObject in managers) {
               
                if (curObject != this &&
                    curObject.lifeTime == this._lifeTime) {
                    curObject.beDestroyed();
                }
                else if (curObject.lifeTime > this._lifeTime) {
                    this.beDestroyed();
                    return;
                }
                
            }
        }
    }

    virtual protected void Update() {
        _lifeTime += Time.deltaTime;
    }

    public void beDestroyed() {
        GameObject.Destroy(gameObject);
    }
}
