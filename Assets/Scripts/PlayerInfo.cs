﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerInfo : Persistant {

    private Text scoreHUDField;
    private Text livesHUDField;

    [SerializeField]
    private int _lives;
    public int lives {
        get { return _lives; }
        set {
            print(value);
            _lives = value;
            livesHUDField.text = value.ToString();
        } 
    }

    [SerializeField]
    private int _score;
    public int score {
        get { return _score; }
        set {
            _score = value;
            scoreHUDField.text = value.ToString();
        }
    }
    // Use this for initialization
    void Start () {

        GameObject scoreHUD_GO = GameObject.Find("/HUDCanvas/score");
        scoreHUDField = scoreHUD_GO.GetComponent<Text>();
        scoreHUDField.text = _score.ToString();

        GameObject livesHUD_GO = GameObject.Find("/HUDCanvas/lives");
        livesHUDField = livesHUD_GO.GetComponent<Text>();
        livesHUDField.text = _lives.ToString();
    }
    
   
}
